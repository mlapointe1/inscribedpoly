===================
Inscribed Polyomino
===================

Convert txt file which content an inscribed polyomino in a rectangle to a tikz file. 

Requierement
~~~~~~~~~~~~

python 

Usage
~~~~~~
To execute the python script, tapped::
    
    python txt2tikz.py input_file output_dir

The input file's format must be:
    - Each symbol represent one cell in the rectangle
        -Default:
            - X  black cell
            - \-  white cell
    - You can add custom colors at the beginning of the file::
    
        #symbol=color
        
       - Write only one color per line
       - Symbol must be a caracter
       - Color use the xcolor package syntax

Example::


            #X=green
            #Y=blue!50
            XY-XY-XY
            Y-XY-XY-
            -XY-XY-X
            XY-XY-XY
            Y-XY-XY-
            -XY-XY-X
            XY-XY-XY
            Y-XY-XY-

To use the tikz file into a latex document, there is to choice::

    \input{output_file.tikz}

or externalise has a pdf file with tikz2pdf_.

.. _tikz2pdf: https://github.com/jeroenjanssens/tikz2pdf



