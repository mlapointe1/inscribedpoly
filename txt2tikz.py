import sys

def rect_dim(lines):
    height = len(lines)
    width = len(lines[0])-1
    return (height,width)

def cell_coord(dim_rect,ind):
    y_up = dim_rect[0] - ind[0]
    y_down = y_up - 1
    x_down = ind[1]
    x_up = x_down + 1
    return (x_down, y_down, x_up, y_up)

def cell_str(cell,coord):
    s = '           \draw['
    s+= cell + '] '
    s+= '('+str(coord[0])+','+str(coord[1])+') rectangle (' +str(coord[2])+','+str(coord[3]) +');\n'
    return s

def write_tikz_str(lines, dim, colors):
    s = '\\begin{center} \n    \\begin{tikzpicture}['
    cmpt = 0
    for sc in colors.keys():
        if(cmpt != 0):
            s+=','
        s += sc + '/.style={fill='+colors[sc][0]+'}'
        cmpt += 1
    s+=']\n      \\begin{scope}[scale=0.50]\n'
    for i in range(dim[0]):
        for j in range(dim[1]):
            c = cell_coord(dim,(i,j))
            s += cell_str(lines[i][j],c)
    s+='        \end{scope}\n    \end{tikzpicture}\n\end{center}'
    return s

def make_color_dict(color):
    color_dict = {}
    color_dict['X'] = ['black']
    color_dict['-'] = ['white']
    for c in color:
        color_dict[c[1]] = [c[3:-1]]
    return color_dict

def main():
    with open(sys.argv[1], 'r') as fichier:
        lines = fichier.readlines()
    k = []
    while(lines[0][0]=='#'):
        k += [lines[0]]
        lines = lines[1:]
    colors = make_color_dict(k)
    dim = rect_dim(lines)
    s = write_tikz_str(lines,dim,colors)
    l = sys.argv[1].split('/')
    r = len(l)
    l = l[r-1].split('.')
    with open(sys.argv[2]+l[0]+'.tikz','w') as fichier:
        fichier.write(s)


if __name__=="__main__":
    main()
